package enums;

public enum SessionState {
    Started, Ended, Paused;
}
